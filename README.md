# Power User With Gnome

![screenshot](https://gitlab.com/avishekdutta531/power-user-with-gnome/-/raw/main/images/screenshot.png)

## Getting Started

1. GTK Theme - Dracula
2. Dock - Plank
3. Shell - Mutter
4. Shell Theme - Matcha Dark Azul
5. Icon Theme - Tela
6. App Launcher - Ulauncher

### Theme Installation and Configuration

- Change the theme of every application from here - https://draculatheme.com/
- Plank Dock - https://launchpad.net/plank
- Ulauncher - https://ulauncher.io/
- Theme for Plank Dock - Orchis
- Tela Icon Theme - https://github.com/vinceliuice/Tela-icon-theme
- Install Gnome Tweak Tool and Enable Gnome Extension
- Extensions - Arcmenu, User Themes, Panel OSD, Blur My Shell, Clipboard Indicator, Frippery Move Clock

## Terminal Customization

1. Shell - ZSH
2. Theme - Dracula
3. Shell Prompt - Oh My ZSH (With Powerlevel10k)

### Installation of Oh My ZSH and Powerlevel10k

- Follow the official documentation to install Oh My ZSH - https://ohmyz.sh/#install
- Install Powerlevel10k for Oh My ZSH - https://github.com/romkatv/powerlevel10k#oh-my-zsh

## Key Bindings

|  Key  |  Command  |
|  ----------------  |  ----------------  |
|  `super + shift + enter`  |  `gnome-terminal`  |
|  `super + shift + c`  |  `closing window`  |
|  `super + shift + t`  |  `opening new tab in terminal`  |
|  `super + shift + w`  |  `closing tab in terminal`  |
|  `super + shift + h`  |  `file manager`  |
|  `right side super`  |  `arc-menu`  |
|  `left side super`  |  `activites overview`  |
|  `super + shift + m`  |  `maximizing window`  |
|  `super + shift + ->`  |  `window split right`  |
|  `super + shift + <-`  |  `window split left`  |

